import 'package:flutter/material.dart';
import 'package:projeto_soma/Theme/Colors_App.dart';
import 'package:projeto_soma/components/button_bar.dart';
import 'package:projeto_soma/components/input_dropdown_button.dart';
import 'package:projeto_soma/components/nav.dart';
import 'package:projeto_soma/components/page_menu.dart';

class PageProdutos extends StatefulWidget {
  @override
  _PageProdutosState createState() => _PageProdutosState();
}

class _PageProdutosState extends State<PageProdutos> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: GestureDetector(
          onTap: () {
            pop(context);
          },
          child: ListTile(
            leading: Icon(
              Icons.keyboard_arrow_left,
              color: ColorsApp.colorApp,
            ),
            title: Text(
              'Produtos',
              style: TextStyle(color: ColorsApp.colorApp),
            ),
          ),
        ),
      ),
      body: Column(children: <Widget>[
        SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            InputDropDownButton(
              text: 'Segmento',
            ),
            InputDropDownButton(
              text: 'Ordenar',
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        SizedBox(
          height: MediaQuery.of(context).size.height - 160,
          child: Padding(
            padding: const EdgeInsets.only(bottom: 60),
            child: PageMenu(),
          ),
        ),
      ]),
      bottomSheet: ComponentBottomNavigationBar(
        colorPedidos: ColorsApp.colorApp,
        colorInicio: Colors.grey,
        colorPerfil: Colors.grey,
      ),
    );
  }
}

import 'package:projeto_soma/models/pedido.dart';
import 'package:rxdart/rxdart.dart';

class EfetuarPagamentoBloc {
  final _numeroDoCartao = BehaviorSubject<String>();
  final _cpf = BehaviorSubject<String>();
  final _dataDeVencimento = BehaviorSubject<String>();
  final _nomeNoCartao = BehaviorSubject<String>();
  final _banco = BehaviorSubject<String>();
  final _codigo = BehaviorSubject<String>();
  final _cpfNaNota = BehaviorSubject<String>();

  Stream<String> get numeroDocartao => _numeroDoCartao.stream;
  Stream<String> get cpf => _cpf.stream;
  Stream<String> get dataDevencimento => _dataDeVencimento.stream;
  Stream<String> get nomeNoCartao => _nomeNoCartao.stream;
  Stream<String> get banco => _banco.stream;
  Stream<String> get codigo => _codigo.stream;
  Stream<String> get cpfNaNota => _cpfNaNota.stream;
 
 

  Function(String) get changeNumeroDoCartao => _numeroDoCartao.sink.add;
  Function(String) get changeCpf => _cpf.sink.add;
  Function(String) get changeDataDeVencimento => _dataDeVencimento.sink.add;
  Function(String) get changeNomeNoCartao => _nomeNoCartao.sink.add;
  Function(String) get changeBanco => _banco.sink.add;
  Function(String) get changeCodigo => _codigo.sink.add;
  Function(String) get changeCpfNaNota => _cpfNaNota.sink.add;

  // serviço de pagamento aqui
submit() {
    Pedido pedido = new Pedido();
    pedido.numeroDoCartao = _numeroDoCartao.value;
    pedido.nomeNoCartao = _nomeNoCartao.value;
    pedido.cpfDoTitular = _cpf.value;
    pedido.dataDeVencimento = _dataDeVencimento.value;
    pedido.banco = _banco.value;
    pedido.codigo = _codigo.value;
    pedido.cpfNaNota = _cpfNaNota.value;
    return pedido.toJson();
}
  void dispose() {
    _numeroDoCartao.close();
    _cpf.close();
    _dataDeVencimento.close();
    _nomeNoCartao.close();
    _banco.close();
    _codigo.close();
    _cpfNaNota.close();
  }
}

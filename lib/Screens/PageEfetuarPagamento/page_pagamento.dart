import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:projeto_soma/Screens/PageEfetuarPagamento/efetuar_pagamento_bloc.dart';
import 'package:projeto_soma/Theme/Colors_App.dart';
import 'package:projeto_soma/components/custom_formulario.dart';
import 'package:projeto_soma/components/nav.dart';


EfetuarPagamentoBloc _efetuarPagamentoBloc = EfetuarPagamentoBloc();
final _formKey = GlobalKey<FormState>();
var cpfController = new MaskedTextController(mask: '000.000.000-00');
var codigoController = new MaskedTextController(mask: '000');
TextEditingController numeroDoCartao = TextEditingController();
var dataDeVencimentoController = TextEditingController();
var bandeiraController = TextEditingController();
var bandeiraUrlController = TextEditingController();
var bancoController = TextEditingController();
var cpfNaNotaController = TextEditingController();
var nomeNoCartao = TextEditingController();


//Regras Cartão de Crédito
var visa = new RegExp(r"^(?:4[0-9]{12}(?:[0-9]{3})?)$");
var master = new RegExp(
    "^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))\$");
var elo = new RegExp(
    "^((((636368)|(438935)|(504175)|(451416)|(636297))\d{0,10})|((5067)|(4576)|(4011))\d{0,12})");

void validarBandeira(text) {
  if (visa.hasMatch(text)) {
    bandeiraController.text = 'Visa';
    bandeiraUrlController.text =
        'https://www.comoinvestirnoexterior.com/wp-content/uploads/2019/05/visa.jpg';
  }
  if (master.hasMatch(text)) {
    bandeiraController.text = 'Master';
    print('master');
    bandeiraUrlController.text =
        'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/Mastercard-logo.svg/200px-Mastercard-logo.svg.png';
  }
  if (elo.hasMatch(text)) {
    bandeiraController.text = 'Elo';
    bandeiraUrlController.text =
        'https://seeklogo.com/images/E/elo-logo-8D71139387-seeklogo.com.jpg';
  }
  
}

class PagePagamento extends StatefulWidget {
  @override
  _PagePagamento createState() => _PagePagamento();
}

class _PagePagamento extends State<PagePagamento> {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: GestureDetector(
          onTap: () {
            pop(context);
          },
          child: ListTile(
            leading: Icon(
              Icons.keyboard_arrow_left,
              color: ColorsApp.colorApp,
            ),
            title: Text(
              'Pagamento OS : 00000',
              style: TextStyle(color: ColorsApp.colorApp),
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: Row(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width - 140,
                      child: CustomFormulario(
                        label: 'Numero do Cartão',
                        controller: numeroDoCartao,
                        keyboardType:TextInputType.number,
                        onChanged: (numeroDoCartao) {                        
                          validarBandeira(numeroDoCartao.text);
                        },
                        bloc: _efetuarPagamentoBloc.changeNumeroDoCartao(numeroDoCartao.text),
                      ),
                    ),
                    const SizedBox(width: 30),
                    Image.network(
                      bandeiraUrlController.text,
                      width: 40,
                      height: 40,
                    ),
                  ],
                ),
              ),
              SizedBox(height: 20),
              ListTile(
                leading: Container(
                  width: MediaQuery.of(context).size.width - 50,
                  child: CustomFormulario(
                    label: 'Nome no cartão',
                    controller: nomeNoCartao,
                    bloc: _efetuarPagamentoBloc
                        .changeNomeNoCartao(nomeNoCartao.text),
                  ),
                ),
              ),
              SizedBox(height: 20),
              ListTile(
                leading: Container(
                  width: MediaQuery.of(context).size.width - 50,
                  child: CustomFormulario(
                    controller: cpfController,
                    keyboardType: TextInputType.number,
                    label: "CPF do titular",
                    bloc: _efetuarPagamentoBloc.changeCpf(cpfController.text),
                  ),
                ),
              ),
              SizedBox(height: 20),
              Padding(
                padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
                child: Row(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width / 2,
                      child: CustomFormulario(
                        label: 'Data de vencimento',
                        keyboardType: TextInputType.number,
                        controller: dataDeVencimentoController,
                        bloc: _efetuarPagamentoBloc.changeDataDeVencimento(
                            dataDeVencimentoController.text),
                      ),
                    ),
                    SizedBox(width: 10),
                    Container(
                      width: MediaQuery.of(context).size.width / 3,
                      child: CustomFormulario(
                        label: 'Código',
                        keyboardType: TextInputType.number,
                        controller: codigoController,
                        bloc: _efetuarPagamentoBloc.changeCodigo(codigoController.text),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 20),
              Padding(
                padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: Row(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width / 3,
                      child: CustomFormulario(
                        label: 'Banco',
                        controller: bancoController,
                        bloc: _efetuarPagamentoBloc.changeBanco(bancoController.text),
                      ),
                    ),
                    SizedBox(height: 5),
                  ],
                ),
              ),
              SizedBox(height: 15),
              ListTile(
                leading: Container(
                  width: MediaQuery.of(context).size.width - 50,
                  child: CustomFormulario(
                    label: 'CPF/CNPJ na nota?',
                    controller: cpfNaNotaController,
                    bloc: _efetuarPagamentoBloc
                        .changeCpfNaNota(cpfNaNotaController.text),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 30),
              ),
              GestureDetector(
                onTap: () {
                 if (_formKey.currentState.validate()) {
                    showDialog(
                      context: context,
                      builder: (BuildContext context){
                          return AlertDialog(
                             title: new Text("Pedido Finalizado!"),
                          content: new Text('${_efetuarPagamentoBloc.submit()}'),
                          );
                      }
                    );
                }
                },
                child: Container(
                  height: 80.0,
                  color: ColorsApp.colorApp,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Text(
                        'Finalizar Pedido',
                        style: TextStyle(color: Colors.yellow),
                      ),
                      Text(
                        'R\$ 354,67',
                        style: TextStyle(color: Colors.yellow),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:projeto_soma/Screens/PageMenu/menu_page.dart';
import 'package:projeto_soma/Theme/Colors_App.dart';
import 'package:projeto_soma/components/input_dropdown_button.dart';

class PageDetalhesProduto extends StatefulWidget {
  @override
  _PageDetalhesProdutoState createState() => _PageDetalhesProdutoState();
}

class _PageDetalhesProdutoState extends State<PageDetalhesProduto> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: ListTile(
          onTap: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => Menupage()));
          },
          leading: Icon(
            Icons.keyboard_arrow_left,
            color: ColorsApp.colorApp,
            size: 30,
          ),
          title: Center(
            child: Text(
              'Detalhes',
              style: TextStyle(color: ColorsApp.colorApp),
            ),
          ),
        ),
      ),
      body: Container(
        child: ListView.builder(
          itemBuilder: (context, index) {
            return Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 50, left: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Cartão de visita Business',
                        style: TextStyle(
                          fontSize: 24,
                          color: ColorsApp.colorApp,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 200,
                  child: Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Container(
                            color: Colors.grey,
                            width: 200.0,
                            child: Icon(Icons.perm_camera_mic, size: 100),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: Container(
                            color: Colors.grey,
                            width: 200.0,
                            child: Icon(Icons.perm_camera_mic, size: 100),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: Container(
                            color: Colors.grey,
                            width: 200.0,
                            child: Icon(Icons.perm_camera_mic, size: 100),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: Container(
                            color: Colors.grey,
                            width: 200.0,
                            child: Icon(Icons.perm_camera_mic, size: 100),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: Container(
                            color: Colors.grey,
                            width: 200.0,
                            child: Icon(Icons.perm_camera_mic, size: 100),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: Container(
                            color: Colors.grey,
                            width: 200.0,
                            child: Icon(Icons.perm_camera_mic, size: 100),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: Container(
                            color: Colors.grey,
                            width: 250.0,
                            child: Icon(Icons.perm_camera_mic, size: 100),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: Container(
                            color: Colors.grey,
                            width: 250.0,
                            child: Icon(Icons.perm_camera_mic, size: 100),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20, left: 30),
                  child: Row(
                    children: <Widget>[
                      Text('Descrição', style: TextStyle(fontSize: 20))
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(width: 1.0, color: ColorsApp.colorApp),
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    width: MediaQuery.of(context).size.width - 60,
                    child: Padding(
                      padding: EdgeInsets.only(left: 20, top: 20, bottom: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text('Tamanho: ',
                                  style: TextStyle(
                                      color: ColorsApp.colorApp,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold)),
                              Text('85 x 85 cm')
                            ],
                          ),
                          SizedBox(height: 5),
                          Row(
                            children: <Widget>[
                              Text('Material: ',
                                  style: TextStyle(
                                      color: ColorsApp.colorApp,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold)),
                              Text('Papel 300g')
                            ],
                          ),
                          SizedBox(height: 5),
                          Row(
                            children: <Widget>[
                              Text('Impressão: ',
                                  style: TextStyle(
                                      color: ColorsApp.colorApp,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold)),
                              Text('Cores, frente e verso')
                            ],
                          ),
                          SizedBox(height: 5),
                          Row(
                            children: <Widget>[
                              Text('Acabamento: ',
                                  style: TextStyle(
                                      color: ColorsApp.colorApp,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold)),
                              Text('Sem acabamento')
                            ],
                          ),
                          SizedBox(height: 5),
                          Row(
                            children: <Widget>[
                              Text('Extras: ',
                                  style: TextStyle(
                                      color: ColorsApp.colorApp,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold)),
                              Text('sem canto arredondado')
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20, left: 30),
                  child: Row(
                    children: <Widget>[
                      Text('Quantidade', style: TextStyle(fontSize: 20))
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: Container(
                    width: MediaQuery.of(context).size.width - 60,
                    child: InputDropDownButton(
                      text: 'Quantidade',
                    ),
                  ),
                ),
                Container(
                  height: 200,
                )
              ],
            );
          },
          itemCount: 1,
        ),
      ),
      bottomSheet: Container(
          height: 80,
          color: Colors.red,
          child: Center(
            child: ListTile(
              leading: Icon(Icons.shopping_cart, color: Colors.yellowAccent),
              title: Text('Adicionar ao carrinho',
                  style: TextStyle(color: Colors.yellowAccent)),
              trailing: Text('RS ' + '354,67',
                  style: TextStyle(color: Colors.yellowAccent, fontSize: 20)),
            ),
          )),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:projeto_soma/Screens/RecoveryPassword/recovery_password_bloc.dart';
import 'package:projeto_soma/components/nav.dart';
import 'package:auto_size_text/auto_size_text.dart';

class PageRecoveryPassword extends StatefulWidget {
  @override
  _PageRecoveryPasswordState createState() => _PageRecoveryPasswordState();
}

class _PageRecoveryPasswordState extends State<PageRecoveryPassword> {
  TextEditingController campoEmail = TextEditingController();
  RecuperacaoSenhaBloc _recuperacaoSenhaBloc = RecuperacaoSenhaBloc();

  var retornoMensagem = '';

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text('Redefinição de senha'),
          backgroundColor: Colors.green,
        ),
        body: Padding(
          padding: EdgeInsets.all(20.0),
          child: Column(
            children: <Widget>[
              Text(
                'Insira seu e-mail abaixo.',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                  'Em seguida enviaremos uma mensagem de confirmação contendo um link para a redefinição da sua senha.'),
              TextFormField(
                textInputAction: TextInputAction.go,
                controller: campoEmail,
                decoration: InputDecoration(
                  suffixIcon: Icon(
                    Icons.person,
                    color: Theme.of(context).primaryColor,
                  ),
                  labelText: 'Digite seu e-mail',
                ),
              ),
              SizedBox(
                height: 20,
              ),
              postRetornoMensagem(),
              buttomEnviar(),
            ],
          ),
        ),
      ),
    );
  }

  postRetornoMensagem() {
    if (retornoMensagem == 'Redefinição de senha enviado com sucesso!') {
      return Container(
        height: 50,
        child: Text(
          retornoMensagem,
          style: TextStyle(color: Colors.green),
        ),
      );
    } else {
      return Container(
        height: 50,
        child: Text(
          retornoMensagem,
          style: TextStyle(color: Colors.red),
        ),
      );
    }
  }

  buttomEnviar() {
    if (retornoMensagem != 'Redefinição de senha enviado com sucesso!') {
      return GestureDetector(
        onTap: () async {
          retornoMensagem =
              await _recuperacaoSenhaBloc.redefinirSenha(campoEmail.text);
          setState(() {});
        },
        child: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 50.0,
                width: 150,
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.all(
                    Radius.circular(5.0),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    AutoSizeText(
                      "Enviar",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    } else {
      return GestureDetector(
        onTap: () {
          pop(context);
        },
        child: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 50.0,
                width: 150,
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.all(
                    Radius.circular(5.0),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    AutoSizeText(
                      "Voltar",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    }
  }
}

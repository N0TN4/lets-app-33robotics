import 'package:firebase_auth/firebase_auth.dart';

class RecuperacaoSenhaBloc {
  static int retornoRecSenha;

  static String retornoRecEmail = '';
  final FirebaseAuth _auth = FirebaseAuth.instance;

  redefinirSenha(String email) async {
    try {
      await _auth.sendPasswordResetEmail(email: email);
      return 'E-mail enviado com sucesso!';
    } catch (e) {
      if (e.message == 'ERROR_INVALID_EMAIL') {
        return 'E-mail ínvalido';
      }
      if (e.message == 'ERROR_USER_NOT_FOUND') {
        return 'Usuário não encontrado!';
      }
      if (e.message == 'ERROR_MISSING_EMAIL') {
        return 'E-mail ínvalido';
      } else {
        return 'Ocorreu um erro, tente novamente';
      }
    }
  }

  void dispose() {}
}

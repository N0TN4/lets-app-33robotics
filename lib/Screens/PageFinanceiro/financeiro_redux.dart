import 'package:projeto_soma/models/financeiro.dart';
import 'package:rxdart/rxdart.dart';

class FinanceiroRedux {
  final _alimento = BehaviorSubject<List<Alimento>>();
  Stream<List<Alimento>> get alimento => _alimento.stream;
  Function(List<Alimento>) get changeAlimento => _alimento.sink.add;

  void dispose() {
    _alimento.close();
  }
}

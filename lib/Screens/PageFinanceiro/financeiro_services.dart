import 'package:projeto_soma/Services/abstract_services.dart';
import 'package:projeto_soma/models/financeiro.dart';

class FinanceiroServices extends AbstractService {
  FinanceiroServices() : super('/default/Alimento');

  Alimento alimento = new Alimento();
  Future<List<Alimento>> getAlimentos(String grupo, String descricao) =>
      Session.get('$api?grupo=$grupo&descricao=$descricao').then((json) {
        return json.length == 0 ? null : fromListJson(json);
      });

  @override
  fromJson(json) {
    return Alimento.fromJson(json);
  }

  fromListJson(json) {
    return alimento.listaAlimento(json);
  }
}

import 'package:projeto_soma/Screens/PageFinanceiro/financeiro_services.dart';

class FinanceiroBloc {
  FinanceiroServices _services = FinanceiroServices();

  getAlimento(financeiroRedux) async {
    var grupo = 'F';
    var descricao = 'lar';

    await _services.getAlimentos(grupo, descricao).then((response) {
      if (response != null) {
        financeiroRedux.changeAlimento(response);
      }
    });
  }
}

import 'package:flutter/material.dart';
import 'package:projeto_soma/Screens/PageFinanceiro/financeiro_bloc.dart';
import 'package:projeto_soma/Screens/PageFinanceiro/financeiro_redux.dart';
import 'package:projeto_soma/components/button_bar.dart';

class FinanceiroPage extends StatefulWidget {
  @override
  _FinanceiroPageState createState() => _FinanceiroPageState();
}

FinanceiroRedux financeiroRedux = FinanceiroRedux();
FinanceiroBloc financeiroBloc = FinanceiroBloc();

final List<String> entries = <String>['A', 'B', 'C'];
final List<int> colorCodes = <int>[600, 500, 100];

class _FinanceiroPageState extends State<FinanceiroPage> {
  @override
  void initState() {
    super.initState();
    financeiroBloc.getAlimento(financeiroRedux);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder(
          stream: financeiroRedux.alimento,
          builder: (context, snapshot) {
            if (snapshot.data != null) {
              return Center(
                child: ListView.separated(
                  padding: const EdgeInsets.all(8),
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      height: 50,
                      color: Colors.amber[colorCodes[index]],
                      child: Center(
                          child: Text('${snapshot.data[index].descricao}')),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) =>
                      const Divider(),
                ),
              );
            } else {
              return ListView.separated(
                padding: const EdgeInsets.all(8),
                itemCount: entries.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    height: 50,
                    color: Colors.amber[colorCodes[index]],
                    child: Center(child: Text('Entry ${entries[index]}')),
                  );
                },
                separatorBuilder: (BuildContext context, int index) =>
                    const Divider(),
              );
            }
          }),
      bottomSheet: ComponentBottomNavigationBar(
          colorInicio: Colors.grey,
          colorPedidos: Colors.grey,
          colorPerfil: Color.fromRGBO(240, 83, 82, 1)),
    );
  }
}

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:projeto_soma/Screens/PagePhone/bloc_phone.dart';
import 'package:projeto_soma/Screens/PagePhone/page_input_code.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:projeto_soma/components/button_theme_geral.dart';
import 'package:projeto_soma/components/input_phone.dart';
import 'package:flutter/services.dart';

class PageInputPhone extends StatefulWidget {
  @override
  PageInputPhoneState createState() => PageInputPhoneState();
}

class PageInputPhoneState extends State<PageInputPhone> {
  final phoneController = MaskedTextController(mask: '(00)00000-0000');
  BlocPhone _blocPhone = BlocPhone();

  static dynamic cod;

  static FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    return Scaffold(
        body: Container(
      color: Colors.yellow,
      child: ListView(
        children: <Widget>[
          Container(
              color: Colors.yellow[500],
              child: new Image.asset('assets/lets.png')),
          SizedBox(
            height: 200,
          ),

          Padding(
            padding: const EdgeInsets.only(right: 100, left: 100),
            child: InputPhone(
              controller: phoneController,
              prefixIcon:
                  Icon(Icons.phone, color: Theme.of(context).primaryColor),
              hintText: 'Digite seu telefone',
              labelText: 'Telefone',
              textInputType: TextInputType.number,
            ),
          ),
          SizedBox(height: 30),
          Padding(
            padding: EdgeInsets.only(right: 100, left: 100),
            child: ButtonThemeGeral(
              color: Colors.blue,
              title: Center(
                child: Text('LOGAR',
                    style: TextStyle(color: Colors.white, fontSize: 16)),
              ),
              onPressed: () {
                _blocPhone.validatePhone(phoneController.text);
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => PageInputCode()));
              },
            ),
          ),
          SizedBox(
            height: 30,
          ),
          // Padding(
          //   padding: EdgeInsets.only(right: 100, left: 100),
          //   child: ButtonThemeGeral(
          //     color: Colors.blue,
          //     title: Center(
          //       child: Text('MENU',
          //           style: TextStyle(color: Colors.white, fontSize: 16)),
          //     ),
          //     onPressed: () async {
          //       // _servicePhone.sendCodeToPhoneNumber(phoneController.text);
          //       Navigator.push(context,
          //           MaterialPageRoute(builder: (context) => Menupage()));
          //     },
          //   ),
          // )
        ],
      ),
    ));
  }
}

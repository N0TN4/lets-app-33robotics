import 'package:projeto_soma/DadosUsuario/dados_usuario.dart';
import 'package:projeto_soma/Screens/PagePhone/phone_services.dart';
import 'package:projeto_soma/ServicePhone/service_phone.dart';
import 'package:projeto_soma/models/usuario.dart';
import 'package:rxdart/rxdart.dart';

class BlocPhone {
  final _errorCode = BehaviorSubject<String>();
  final _validaPhone = BehaviorSubject<bool>();
  Stream<String> get errorCode => _errorCode.stream;
  Stream<bool> get validaPhone => _validaPhone.stream;
  Function(String) get changeErrorCode => _errorCode.sink.add;
  Function(bool) get changeValidaPhone => _validaPhone.sink.add;

  ServicePhone _servicePhone = ServicePhone();
  PhoneServices _phoneServices = PhoneServices();

  //Verifica numero se existe cadastro na base;
  void validatePhone(String phone) async {
    changeErrorCode('');
    var _phone =
        phone.replaceAll('(', '').replaceAll(')', '').replaceAll('-', '');
    await _phoneServices.checkPhone(_phone).then((response) {
      try {
        if (response != null) {
          Usuario _usuario = new Usuario();
          _usuario.id = response['CLIENTE'];
          _usuario.nome = response["NOME"];
          _usuario.cpfcnpj = response['CNPJCPF'];
          _usuario.endereco = response['ENDERECO'];
          _usuario.numero = response['NUMERO'];
          _usuario.complemento = response['COMPLEMENTO'];
          _usuario.cidade = response['CIDADE'];
          _usuario.estado = response['UF'];
          _usuario.celular = response['CELULAR'];
          _usuario.email = response['EMAIL'];
          DadosUsuario.usario = _usuario;
          // Encontrando registro envia Token de validação;
          return sendToken(phone, changeErrorCode, changeValidaPhone);
        }
      } catch (e) {
        //Tratamento de erros;
        changeErrorCode(e.message);
      }
    });
  }

  void sendToken(
      String phone, Function changeErrorCode, Function changeValidaPhone) {
    _servicePhone.sendCodeToPhoneNumber(
        phone, changeErrorCode, changeValidaPhone);
  }

  verifyBloc(String codigo) async {
    try {
      var user;
      changeErrorCode('');
      await _servicePhone
          .verifyCode(codigo, ServicePhone.cod, changeErrorCode)
          .then((response) {
        if (response != null) {
          user = response.user;
        }
      });
      return user;
    } catch (e) {
      print(e);
    }
  }

  void dispose() {
    _errorCode.close();
    _validaPhone.close();
  }
}

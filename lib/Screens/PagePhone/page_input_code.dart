import 'dart:io';

import 'package:flutter/material.dart';
import 'package:projeto_soma/Screens/PageCadastroUser/page_cadastro_user.dart';
import 'package:projeto_soma/Screens/PageMenu/menu_page.dart';
import 'package:projeto_soma/Screens/PagePhone/bloc_phone.dart';

import 'package:projeto_soma/ServicePhone/service_phone.dart';
import 'package:projeto_soma/components/button_theme_geral.dart';

class PageInputCode extends StatefulWidget {
  @override
  PageInputCodeState createState() => PageInputCodeState();
}

class PageInputCodeState extends State<PageInputCode> {
  FocusNode _confirmCodFocus = new FocusNode();
  ServicePhone servicePhone = ServicePhone();

  final TextEditingController codigoController = TextEditingController();
  GlobalKey<FormState> key = GlobalKey<FormState>();
  BlocPhone _blocPhone = BlocPhone();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _confirmCodFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Form(
          key: key,
          child: Container(
            color: Colors.yellow,
            alignment: Alignment.center,
            padding: EdgeInsets.symmetric(horizontal: 32.0),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    color: Colors.yellow[500],
                    child: new Image.asset('assets/lets.png')),
                SizedBox(height: 200),
                TextFormField(
                  validator: (value) {
                    if (value == '') {
                      return 'Campo não pode ser nulo';
                    } else {
                      return null;
                    }
                  },
                  controller: codigoController,
                  keyboardType: TextInputType.number,
                ),
                SizedBox(height: 32.0),
                StreamBuilder(
                    stream: BlocPhone().errorCode,
                    builder: (context, snapshot) {
                      if (snapshot.data != null) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(snapshot.data.toString(),
                                style:
                                    TextStyle(color: Colors.red, fontSize: 12))
                          ],
                        );
                      } else {
                        return Container();
                      }
                    }),
                SizedBox(
                  height: 10,
                ),
                _buildButton(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildButton() {
    return Hero(
      tag: 'btn',
      child: Container(
        width: 200,
        child: ButtonThemeGeral(
          color: Colors.blue,
          onPressed: verify,
          title: Center(
            child: Text(
              'VERIFICAR',
              style: TextStyle(
                color: Colors.white,
                fontSize: 16.0,
              ),
            ),
          ),
        ),
      ),
    );
  }

  void verify() async {
    if (key.currentState.validate()) {
      key.currentState.save();
      if (Platform.isIOS) {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => Menupage()));
      } else {
        var user = await _blocPhone.verifyBloc(codigoController.text);
        if (user != null) {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Menupage()));
        }
      }
    }
  }
}

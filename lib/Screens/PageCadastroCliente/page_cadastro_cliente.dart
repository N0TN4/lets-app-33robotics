import 'package:flutter/material.dart';
import 'package:projeto_soma/Theme/Colors_App.dart';
import 'package:projeto_soma/components/inputEmailText.dart';
import 'package:projeto_soma/components/inputTextForField.dart';
import 'package:projeto_soma/components/nav.dart';

class PageCadastroCliente extends StatefulWidget {
  @override
  _PageCadastroClienteState createState() => _PageCadastroClienteState();
}

class _PageCadastroClienteState extends State<PageCadastroCliente> {
  final companyNameController = TextEditingController();
  final cpfCnpjController = TextEditingController();
  final addressController = TextEditingController();
  final numberController = TextEditingController();
  final complementController = TextEditingController();
  final cityController = TextEditingController();
  final stateController = TextEditingController();
  final websiteController = TextEditingController();
  final contactController = TextEditingController();
  final phoneController = TextEditingController();
  final emailController = TextEditingController();

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        title: Text(
          'Cadastrar Cliente',
          style: TextStyle(color: ColorsApp.colorApp),
        ),
      ),
      body: ListView(
        children: <Widget>[
          Center(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.account_circle, size: 80),
                  SizedBox(height: 40),
                  Padding(
                    padding: EdgeInsets.only(left: 15, right: 15),
                    child: InputTextFormField(
                      hintText: 'Nome da empresa',
                      controller: companyNameController,
                    ),
                  ),
                  SizedBox(height: 20),
                  Padding(
                    padding: EdgeInsets.only(left: 15, right: 15),
                    child: InputTextFormField(
                      hintText: 'CNPJ/CPF',
                      controller: cpfCnpjController,
                    ),
                  ),
                  SizedBox(height: 20),
                  Padding(
                    padding: EdgeInsets.only(left: 15, right: 15),
                    child: InputTextFormField(
                      hintText: 'Endereço',
                      controller: addressController,
                    ),
                  ),
                  SizedBox(height: 20),
                  ListTile(
                    leading: Container(
                      width: MediaQuery.of(context).size.width / 3,
                      child: InputTextFormField(
                        hintText: 'Número',
                        controller: numberController,
                      ),
                    ),
                    trailing: Container(
                      width: MediaQuery.of(context).size.width / 3,
                      child: InputTextFormField(
                        hintText: 'Complemento',
                        controller: complementController,
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  ListTile(
                    leading: Container(
                      width: MediaQuery.of(context).size.width / 3,
                      child: InputTextFormField(
                        hintText: 'Cidade',
                        controller: cityController,
                      ),
                    ),
                    trailing: Container(
                      width: MediaQuery.of(context).size.width / 3,
                      child: InputTextFormField(
                        hintText: 'Estado',
                        controller: stateController,
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  Padding(
                    padding: EdgeInsets.only(left: 15, right: 15),
                    child: InputTextFormField(
                      hintText: 'Website',
                      controller: websiteController,
                    ),
                  ),
                  SizedBox(height: 20),
                  Padding(
                    padding: EdgeInsets.only(left: 15, right: 15),
                    child: InputTextFormField(
                      hintText: 'Contato. Ex.: Pedro',
                      controller: contactController,
                    ),
                  ),
                  SizedBox(height: 20),
                  Padding(
                    padding: EdgeInsets.only(left: 15, right: 15),
                    child: InputTextFormField(
                      hintText: 'Telefone',
                      controller: phoneController,
                    ),
                  ),
                  SizedBox(height: 20),
                  Padding(
                    padding: EdgeInsets.only(left: 15, right: 15),
                    child: InputEmailText(
                      hintText: 'Email',
                      controller: emailController,
                    ),
                  ),
                  SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          RaisedButton(
                            child: Text('CANCELAR'),
                            textColor: Colors.white,
                            color: Color.fromRGBO(153, 28, 16, 1),
                            onPressed: () {
                              pop(context);
                            },
                          ),
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          RaisedButton(
                            child: Text('SALVAR'),
                            textColor: Colors.white,
                            color: Color.fromRGBO(7, 75, 120, 1),
                            onPressed: () {
                              Navigator.pushNamed(context, '/login');
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

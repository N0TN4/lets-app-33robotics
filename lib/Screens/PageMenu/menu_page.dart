import 'package:flutter/material.dart';
import 'package:projeto_soma/components/button_bar.dart';
import 'package:projeto_soma/components/page_menu.dart';

class Menupage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image(
              image: AssetImage('assets/lets_menu.jpg'),
            ),
            SizedBox(
              width: 50,
            )
          ],
        ),
      ),
      body: Padding(
        padding: EdgeInsets.only(bottom: 80),
        child: PageMenu(),
      ),
      bottomSheet: ComponentBottomNavigationBar(
        colorInicio: Color.fromRGBO(240, 83, 82, 1),
        colorPedidos: Colors.grey,
        colorPerfil: Colors.grey,
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:projeto_soma/Screens/PageEfetuarPagamento/page_pagamento.dart';
import 'package:projeto_soma/Theme/Colors_App.dart';
import 'package:projeto_soma/components/nav.dart';

class PageCarrinho extends StatefulWidget {
  @override
  _PageCarrinhoState createState() => _PageCarrinhoState();
}

class _PageCarrinhoState extends State<PageCarrinho> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: GestureDetector(
          onTap: () {
            pop(context);
          },
          child: ListTile(
            leading: Icon(
              Icons.keyboard_arrow_left,
              color: ColorsApp.colorApp,
            ),
            title: Text(
              'Carrinho',
              style: TextStyle(color: ColorsApp.colorApp),
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
              child: Column(
          children: <Widget>[
            Center(child: Text('Conteúdo'),),
            
           
          ],
        ),
      ),
      bottomNavigationBar:  GestureDetector(
              onTap: () {
               Navigator.push(context, 
               MaterialPageRoute(builder: (context) => PagePagamento()));
              },
              child: Container(
                height: 150.0,
                color: ColorsApp.colorApp,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Text(
                      'Efetuar pagamento',
                      style: TextStyle(color: Colors.yellow),
                    ),
                    Text(
                      'R\$ 00,00',
                      style: TextStyle(color: Colors.yellow),
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}

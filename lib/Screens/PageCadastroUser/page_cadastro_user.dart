import 'dart:io';

import 'package:flutter/material.dart';
import 'package:projeto_soma/DadosUsuario/dados_usuario.dart';
import 'package:projeto_soma/Screens/PageCadastroUser/cadastro_bloc.dart';
import 'package:projeto_soma/Theme/Colors_App.dart';
import 'package:projeto_soma/components/button_bar.dart';
import 'package:projeto_soma/components/inputEmailText.dart';
import 'package:projeto_soma/components/inputTextForField.dart';
import 'package:projeto_soma/models/usuario.dart';

class PageCadastroUser extends StatefulWidget {
  final dynamic user;
  const PageCadastroUser(this.user);

  @override
  _PageCadastroUser createState() => _PageCadastroUser();
}

class _PageCadastroUser extends State<PageCadastroUser> {
  @override
  void initState() {
    if (Platform.isIOS) {
    } else {
      Usuario usuario = DadosUsuario.usario;
      nomeController.text = usuario.nome;
      cpfcnpjController.text = usuario.cpfcnpj;
      enderecoController.text = usuario.endereco;
      numeroController.text = usuario.numero;
      complementController.text = usuario.complemento;
      cidadeController.text = usuario.cidade;
      estadoController.text = usuario.estado;
      emailController.text = usuario.email;
    }
    super.initState();
  }

  //valida os campos se estão OK
  final nomeController = TextEditingController();
  final cpfcnpjController = TextEditingController();
  final enderecoController = TextEditingController();
  final numeroController = TextEditingController();
  final complementController = TextEditingController();
  final cidadeController = TextEditingController();
  final estadoController = TextEditingController();
  final phoneController = TextEditingController();
  final emailController = TextEditingController();
  CadastroBloc _cadastroBloc = CadastroBloc();

  Widget build(BuildContext context) {
    //phoneController.text = widget.user.phoneNumber.toString();
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 50,
            ),
            Text(
              'Perfil',
              style: TextStyle(color: Colors.white),
            ),
            SizedBox(
              width: 50,
            )
          ],
        ),
        elevation: 0,
        automaticallyImplyLeading: false,
        backgroundColor: ColorsApp.colorApp,
        actions: <Widget>[
          GestureDetector(
            onTap: () {
              _cadastroBloc.submit();
              print('Salvar Perfil');
            },
            child: Padding(
              padding: EdgeInsets.only(right: 10.0),
              child: Icon(Icons.check, size: 40, color: Colors.white),
            ),
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: ListView(children: <Widget>[
        Center(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.account_circle, size: 80),
                SizedBox(height: 40),
                Padding(
                  padding: EdgeInsets.only(right: 15, left: 15),
                  child: InputTextFormField(
                    bloc: _cadastroBloc.changeNomeusuario(nomeController.text),
                    hintText: 'Nome completo',
                    controller: nomeController,
                  ),
                ),
                SizedBox(height: 20),
                Padding(
                  padding: EdgeInsets.only(right: 15, left: 15),
                  child: InputTextFormField(
                    bloc: _cadastroBloc.changeCpfCnpj(cpfcnpjController.text),
                    textInputType: TextInputType.number,
                    hintText: 'CPF/CNPJ',
                    controller: cpfcnpjController,
                  ),
                ),
                SizedBox(height: 20),
                Padding(
                  padding: EdgeInsets.only(right: 15, left: 15),
                  child: InputTextFormField(
                    bloc: _cadastroBloc.changeEndereco(enderecoController.text),
                    valueController: enderecoController.text,
                    hintText: 'Endereço',
                    controller: enderecoController,
                  ),
                ),
                SizedBox(height: 20),
                ListTile(
                  leading: Container(
                    width: MediaQuery.of(context).size.width / 3,
                    child: InputTextFormField(
                      bloc: _cadastroBloc.changeNumero(numeroController.text),
                      textInputType: TextInputType.number,
                      hintText: 'Número',
                      controller: numeroController,
                    ),
                  ),
                  trailing: Container(
                    width: MediaQuery.of(context).size.width / 3,
                    child: InputTextFormField(
                      bloc: _cadastroBloc
                          .changeComplemento(complementController.text),
                      hintText: 'Complemento',
                      controller: complementController,
                    ),
                  ),
                ),
                SizedBox(height: 10),
                ListTile(
                  leading: Container(
                    width: MediaQuery.of(context).size.width / 3,
                    child: InputTextFormField(
                      bloc: _cadastroBloc.changeCidade(cidadeController.text),
                      hintText: 'Cidade',
                      controller: cidadeController,
                    ),
                  ),
                  trailing: Container(
                    width: MediaQuery.of(context).size.width / 3,
                    child: InputTextFormField(
                      bloc: _cadastroBloc.changeEstado(estadoController.text),
                      hintText: 'Estado',
                      controller: estadoController,
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Padding(
                  padding: EdgeInsets.only(right: 15, left: 15),
                  child: InputTextFormField(
                    bloc: _cadastroBloc.changeTelefone(phoneController.text),
                    enabled: false,
                    hintText: 'Telefone',
                    controller: phoneController,
                  ),
                ),
                SizedBox(height: 20),
                Padding(
                  padding: EdgeInsets.only(right: 15, left: 15),
                  child: InputEmailText(
                    hintText: 'Email',
                    controller: emailController,
                  ),
                ),
                SizedBox(height: 20),
              ],
            ),
          ),
        ),
      ]),
      bottomNavigationBar: ComponentBottomNavigationBar(
          colorInicio: Colors.grey,
          colorPedidos: Colors.grey,
          colorPerfil: Color.fromRGBO(240, 83, 82, 1)),
    );
  }
}

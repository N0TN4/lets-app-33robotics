import 'package:flutter/material.dart';
import 'package:projeto_soma/Theme/Colors_App.dart';
import 'package:projeto_soma/components/button_bar.dart';
import 'package:projeto_soma/components/input_dropdown_button.dart';

class PagePedidos extends StatefulWidget {
  @override
  _PagePedidosState createState() => _PagePedidosState();
}

class _PagePedidosState extends State<PagePedidos> {
  Cliente cliente = new Cliente();

  List<Cliente> pedidos = new List<Cliente>();
  @override
  void initState() {
    getpedidos();
    super.initState();
  }

  getpedidos() {
    cliente.cliente = 'Lets';
    cliente.contato = 'Joao';
    cliente.data = '23/08/2019';
    cliente.os = '000351';
    pedidos.add(cliente);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: Center(
          child: Text(
            'Meus Pedidos',
            style: TextStyle(color: ColorsApp.colorApp),
          ),
        ),
      ),
      body: Column(children: <Widget>[
        SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            InputDropDownButton(
              text: 'OS',
            ),
            InputDropDownButton(
              text: 'Destaque',
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          height: MediaQuery.of(context).size.height - 160,
          child: Padding(
            padding: const EdgeInsets.only(bottom: 60),
            child: ListView.builder(
              itemCount: pedidos.length,
              itemBuilder: (context, index) {
                if (pedidos.length <= 0) {
                  return Container();
                } else {
                  return ListTile(
                    leading: Icon(
                      Icons.account_circle,
                      size: 80,
                    ),
                    title: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: 15),
                        Text('Cliente: ' + pedidos[index].cliente),
                      ],
                    ),
                    subtitle: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: 2),
                        Text('Contato: ' + pedidos[index].contato),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width - 220,
                              child: Text('OS: ' + pedidos[index].os),
                            ),
                          ],
                        ),
                        Text('Data: ' + pedidos[index].data)
                      ],
                    ),
                    trailing: Icon(
                      Icons.update,
                      size: 30,
                    ),
                  );
                }
              },
            ),
          ),
        ),
      ]),
      bottomSheet: ComponentBottomNavigationBar(
        colorPedidos: ColorsApp.colorApp,
        colorInicio: Colors.grey,
        colorPerfil: Colors.grey,
      ),
    );
  }
}

class Cliente {
  String cliente;
  String contato;
  String os;
  String data;
}

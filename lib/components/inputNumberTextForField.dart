import 'package:flutter/material.dart';

class InputNumberTextForField extends StatelessWidget {
  final Key key;
  final Widget prefixIcon;
  final Widget suffixIcon;
  final String hintText;
  final String labelText;
  final String suffixText;
  final TextEditingController controller;
  final TextInputAction textInputAction;
  final TextInputType textInputType;
  final bool enabled;
  final FocusNode focusNode;
  final Function onFieldSubmitted;
  final int maxLines;
  final bool required;
  final TextCapitalization textCapitalization;
  final String initalValue;
  final String valueController;
  final dynamic bloc;

  InputNumberTextForField(
      {this.key,
      this.prefixIcon,
      this.initalValue,
      this.suffixIcon,
      this.hintText,
      this.labelText,
      this.suffixText,
      this.controller,
      this.textInputAction,
      this.textInputType,
      this.enabled,
      this.focusNode,
      this.onFieldSubmitted,
      this.maxLines,
      this.required,
      this.textCapitalization,
      this.valueController,
      this.bloc});

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return TextFormField(
      maxLines: maxLines,
      enabled: enabled,
      focusNode: focusNode,
      textCapitalization: textCapitalization == null
          ? TextCapitalization.none
          : textCapitalization,
      onFieldSubmitted: onFieldSubmitted,
      textInputAction: textInputAction,
      keyboardType: TextInputType.number,
      controller: controller,
      validator: required == true
          ? (value) => value.isEmpty ? 'campo obrigatorio' : null
          : null,
      decoration: InputDecoration(
          labelStyle: TextStyle(height: 0.0, color: theme.primaryColor),
          contentPadding:
              EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
          suffixIcon: suffixIcon,
          suffixText: suffixText,
          prefixIcon: prefixIcon,
          hintText: hintText,
          labelText: labelText
          // labelText: labelText
          ),
    );
  }
}

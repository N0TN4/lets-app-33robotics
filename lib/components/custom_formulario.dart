import 'package:flutter/material.dart';

class CustomFormulario extends StatefulWidget {
  final String label;
  final TextInputType keyboardType;
  final TextEditingController controller;
  final Function onChanged;
  final bool isObcure;
  final FocusNode focusNode;
  final FocusNode nextFocus;
  final Function submitForm;
  final String errorText;
  final dynamic bloc;

  CustomFormulario(
      {this.focusNode,
      this.label,
      this.keyboardType,
      this.controller,
      this.onChanged,
      this.isObcure,
      this.nextFocus,
      this.submitForm,
      this.errorText,
      this.bloc});

  @override
  MyCustomFormState createState() {
    return MyCustomFormState(this.label,this.onChanged, this.keyboardType, this.controller);
  }


}

class MyCustomFormState extends State<CustomFormulario>{
  String txt;
  final Function onChanged;
  final TextInputType keyboardType;
  final TextEditingController controller;
  
  MyCustomFormState(this.txt, this.onChanged, this.keyboardType, this.controller);

  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      validator: (value) {
        if (value.isEmpty) {
          return 'Esse campo não pode estar vazio.';
        }
        return null;
      },
      controller: controller,
      keyboardType: keyboardType,
      onChanged: onChanged,
      decoration: InputDecoration(
      hintText: txt,
        ),
    
    );
  }
}

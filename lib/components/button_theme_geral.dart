import 'package:flutter/material.dart';

class ButtonThemeGeral extends StatelessWidget {
  final Key key;
  final bool alignedDropdown;
  final Color buttonColor;
  final Widget child;
  final ColorScheme colorScheme;
  final Color disabledColor;
  final Color focusColor;
  final double height;
  final Color highlightColor;
  final Color hoverColor;
  final ButtonBarLayoutBehavior layoutBehavior;
  final MaterialTapTargetSize materialTapTargetSize;
  final double minWidth;
  final EdgeInsetsGeometry padding;
  final ShapeBorder shape;
  final Color splashColor;
  final ButtonTextTheme textTheme;
  final Function onPressed;
  final Widget leading;
  final Widget title;
  final Color color;

  ButtonThemeGeral(
      {this.key,
      this.alignedDropdown,
      this.buttonColor,
      this.child,
      this.colorScheme,
      this.disabledColor,
      this.focusColor,
      this.height,
      this.highlightColor,
      this.hoverColor,
      this.layoutBehavior,
      this.materialTapTargetSize,
      this.minWidth,
      this.padding,
      this.shape,
      this.splashColor,
      this.textTheme,
      this.onPressed,
      this.leading,
      this.title,
      this.color});

  @override
  Widget build(BuildContext context) {
    return new ButtonTheme(
      child: RaisedButton(
        color: color,
        onPressed: onPressed,
        child: ListTile(leading: leading, title: title),
      ),
    );
  }
}

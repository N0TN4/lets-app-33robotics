import 'package:flutter/material.dart';

class InputPhone extends StatelessWidget {
  final Key key;
  final Widget prefixIcon;
  final Widget suffixIcon;
  final String hintText;
  final String labelText;
  final String suffixText;
  final TextEditingController controller;
  final TextInputAction textInputAction;
  final TextInputType textInputType;
  final String initialText;
  final bool enabled;
  final FocusNode focusNode;
  final Function onChanged;
  final int maxLines;
  final String errorText;
  final TextCapitalization textCapitalization;

  InputPhone(
      {this.key,
      this.prefixIcon,
      this.suffixIcon,
      this.hintText,
      this.labelText,
      this.suffixText,
      this.textInputAction,
      this.textInputType,
      this.enabled,
      this.focusNode,
      this.onChanged,
      this.initialText,
      this.controller,
      this.maxLines,
      this.errorText,
      this.textCapitalization});

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return TextField(
      maxLines: maxLines,
      textCapitalization: textCapitalization == null
          ? TextCapitalization.none
          : textCapitalization,
      enabled: enabled,
      focusNode: focusNode,
      onChanged: onChanged,
      textInputAction: textInputAction,
      keyboardType: textInputType,
      controller: controller,
      decoration: InputDecoration(
        labelStyle: TextStyle(height: 0.0, color: theme.primaryColor),
        contentPadding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
        suffixIcon: suffixIcon,
        suffixText: suffixText,
        prefixIcon: prefixIcon,
        hintText: hintText,
        labelText: labelText,
        errorText: errorText,
      ),
    );
  }
}

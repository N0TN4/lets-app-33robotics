import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:projeto_soma/Screens/PageCadastroUser/page_cadastro_user.dart';
import 'package:projeto_soma/Screens/PageMenu/menu_page.dart';
import 'package:projeto_soma/Screens/PagePedidos/page_pedidos.dart';

class ComponentBottomNavigationBar extends StatelessWidget {
  final Key key;
  final Color fixedColor;
  final Color backgroundColor;
  final Color selectedItemColor;
  final Color unselectedItemColor;
  final double selectedFontSize;
  final double unselectedFontSize;
  final double elevation;
  final double iconSize;
  final IconThemeData selectIconTheme;
  final IconThemeData unselectIconTheme;
  final bool showSelectedLabels;
  final bool showUnselectedLabels;
  final List items;
  final ValueChanged onTap;
  final TextStyle selectLabelStyle;
  final TextStyle unselectedLabelStyle;
  final BottomNavigationBarType type;
  final int currentIndex;
  final Type runtimeType;
  final Color colorInicio;
  final Color colorPedidos;
  final Color colorPerfil;

  ComponentBottomNavigationBar(
      {this.key,
      this.fixedColor,
      this.backgroundColor,
      this.selectedItemColor,
      this.unselectedItemColor,
      this.selectedFontSize,
      this.unselectedFontSize,
      this.elevation,
      this.iconSize,
      this.selectIconTheme,
      this.unselectIconTheme,
      this.showSelectedLabels,
      this.showUnselectedLabels,
      this.items,
      this.onTap,
      this.selectLabelStyle,
      this.unselectedLabelStyle,
      this.type,
      this.currentIndex,
      this.runtimeType,
      this.colorInicio,
      this.colorPedidos,
      this.colorPerfil});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      child: BottomNavigationBar(
        onTap: (value) {
          switch (value) {
            case 0:
              print('Inicio');
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Menupage()));
              break;
            case 1:
              print('Pedidos');
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => PagePedidos()));
              break;
            case 2:
              print('perfil');
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => PageCadastroUser('')));
              break;
            default:
          }
        },
        elevation: 50,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.th, color: colorInicio),
            title: Text('Início', style: TextStyle(color: colorInicio)),
          ),
          BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.clipboardList, color: colorPedidos),
            title: Text('Pedidos', style: TextStyle(color: colorPedidos)),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.flash_on, color: colorPerfil),
            title: Text('Perfil', style: TextStyle(color: colorPerfil)),
          ),
        ],
      ),
    );
  }
}

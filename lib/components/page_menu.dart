import 'package:flutter/material.dart';
import 'package:projeto_soma/Screens/PageCadastroCliente/page_cadastro_cliente.dart';
import 'package:projeto_soma/Screens/PageCarrinho/page_carrinho.dart';
import 'package:projeto_soma/Screens/PageDetalhesProduto/page_detalhe_produto.dart';
import 'package:projeto_soma/Screens/PageGrupos/page_grupos.dart';
import 'package:projeto_soma/Screens/PagePedidos/page_pedidos.dart';
import 'package:projeto_soma/Screens/PageProdutos/page_produtos.dart';

class PageMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView.count(
        shrinkWrap: true,
        crossAxisCount: 2,
        children: List.generate(opcoes.length, (index) {
          return Center(
            child: OpcaoCard(opcao: opcoes[index]),
          );
        }));
  }
}

class Opcao {
  const Opcao({this.titulo, this.icon});
  final String titulo;
  final IconData icon;
}

const List<Opcao> opcoes = const <Opcao>[
  const Opcao(titulo: 'Busca e Produtos', icon: Icons.store),
  const Opcao(titulo: 'Carrinho', icon: Icons.add_shopping_cart),
  const Opcao(titulo: 'Cadastro Clientes', icon: Icons.people),
  const Opcao(titulo: 'Meus Pedidos', icon: Icons.assignment),
  const Opcao(titulo: 'Meus Pedidos', icon: Icons.assignment),
  const Opcao(titulo: 'Minha Carteira', icon: Icons.payment),
];

class OpcaoCard extends StatelessWidget {
  const OpcaoCard({Key key, this.opcao}) : super(key: key);
  final Opcao opcao;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: GestureDetector(
        onTap: () {
          switch (opcao.titulo) {
            case 'Busca e Produtos':
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => PageProdutos()));
              break;
            case 'Carrinho':
               Navigator.push(context,
                   MaterialPageRoute(builder: (context) => PageCarrinho()));
              break;
            case 'Cadastro Clientes':
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => PageCadastroCliente()));
              break;
            case 'Meus Pedidos':
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => PagePedidos()));
              break;
            case 'Grupos':
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Pagegrupos()));
              break;
            case 'Detalhes Produto':
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => PageDetalhesProduto()));
              break;

            default:
          }
        },
        child: Card(
            color: Color.fromRGBO(240, 83, 82, 1),
            child: Center(
              child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      opcao.icon,
                      size: 80.0,
                      color: Colors.white,
                    ),
                    Text(
                      opcao.titulo,
                      style: TextStyle(fontSize: 16, color: Colors.white),
                    ),
                  ]),
            )),
      ),
    );
  }
}

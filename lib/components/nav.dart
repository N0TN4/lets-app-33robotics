import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

Future push(BuildContext context, Widget page) {
  return Navigator.push(context, MaterialPageRoute(builder: (context) {
    return page;
  }));
}

Future pushReplacement(BuildContext context, Widget page) {
  return Navigator.pushReplacement(context,
      MaterialPageRoute(builder: (context) {
    return page;
  }));
}

Future pushReplacementNamed(BuildContext context, String route) {
  return Navigator.pushReplacementNamed(context, route);
}

Future pushNamedAndRemoveUntil(BuildContext context, String route) {
  return Navigator.of(context)
      .pushNamedAndRemoveUntil(route, (Route<dynamic> route) => false);
}

Future pushAnimation(
    //mudar de página com animação com o package page transition
    BuildContext context,
    PageTransitionType pageTransitionType,
    Widget page) {
  return Navigator.push(
      context, PageTransition(type: pageTransitionType, child: page));
}

bool pop<T extends Object>(context, [T result]) {
  return Navigator.pop(context, result);
}

Future popUntil(BuildContext context, String route) async {
  return Navigator.popUntil(context, ModalRoute.withName(route));
}

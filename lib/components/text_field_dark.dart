import 'package:flutter/material.dart';
import 'package:projeto_soma/components/ensure_visible.dart';

class TextFieldDark extends StatelessWidget {
  final String labelText;
  final TextInputType keyboardType;
  final TextEditingController controller;
  final Function onChanged;
  final bool isObcure;
  final FocusNode focusNode;
  final FocusNode nextFocus;
  final Function submitForm;
  final String errorText;

  TextFieldDark(
      {this.focusNode,
      this.labelText,
      this.keyboardType,
      this.controller,
      @required this.onChanged,
      this.isObcure,
      this.nextFocus,
      this.submitForm,
      this.errorText});

  @override
  Widget build(BuildContext context) {
    return EnsureVisibleWhenFocused(
      focusNode: focusNode,
      child: TextField(
        focusNode: focusNode,
        onChanged: onChanged,
        style: new TextStyle(color: Colors.white),
        cursorColor: Colors.white,
        obscureText: isObcure,
        controller: controller,
        keyboardType: keyboardType,
        onEditingComplete: () {
          if (nextFocus != null) {
            _fieldFocusChange(context, focusNode, nextFocus);
          } else {
            focusNode.unfocus();
            submitForm();
          }
        },
        decoration: InputDecoration(
            fillColor: Color(0xFF001D34),
            filled: true,
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent),
            ),
            labelStyle: TextStyle(color: Colors.grey[200]),
            labelText: labelText,
            errorText: errorText
            // border:
            ),
      ),
    );
  }

  void _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }
}

class TextFormFieldDark extends StatelessWidget {
  final String labelText;
  final TextInputType keyboardType;
  final TextEditingController controller;
  final Function onSaved;
  final bool isObcure;
  final FocusNode focusNode;
  final FocusNode nextFocus;
  final Function submitForm;

  final bool needValidator;

  TextFormFieldDark(
      {this.focusNode,
      this.labelText,
      this.keyboardType,
      this.controller,
      @required this.onSaved,
      this.isObcure,
      this.nextFocus,
      this.submitForm,
      @required this.needValidator});

  @override
  Widget build(BuildContext context) {
    return EnsureVisibleWhenFocused(
      focusNode: focusNode,
      child: TextFormField(
        validator: (value) {
          if (needValidator) {
            if (value.isEmpty) {
              return 'Por favor, preencha este campo.';
            } else {
              return null;
            }
          } else {
            return null;
          }
        },
        focusNode: focusNode,
        onSaved: onSaved,
        style: new TextStyle(color: Colors.white),
        cursorColor: Colors.white,
        obscureText: isObcure,
        controller: controller,
        keyboardType: keyboardType,
        onEditingComplete: () {
          if (nextFocus != null) {
            _fieldFocusChange(context, focusNode, nextFocus);
          } else {
            focusNode.unfocus();
            submitForm();
          }
        },
        decoration: InputDecoration(
            fillColor: Color(0xFF001D34),
            filled: true,
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent),
            ),
            labelStyle: TextStyle(color: Colors.grey[200]),
            labelText: labelText
            // border:
            ),
      ),
    );
  }

  void _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }
}

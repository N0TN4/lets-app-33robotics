import 'package:firebase_auth/firebase_auth.dart';

class ServicePhone {
  static FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  static String cod;

  /// Sends the code to the specified phone number.
  void sendCodeToPhoneNumber(String phoneController, Function changeErrorCode,
      Function changeValidaPhone) async {
    try {
      final PhoneVerificationCompleted verificationCompleted = (user) {
        print(user);
      };

      final PhoneVerificationFailed verificationFailed =
          (AuthException authException) {
        if (authException.code == 'quotaExceeded') {
          changeErrorCode('Muitas requisições, tente novamente mais tarde!');
        }
        print(authException.message);
      };

      PhoneCodeSent codeSent =
          (String verificationId, [int forceResendingToken]) async {
        cod = verificationId;
      };

      final PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
          (String verificationId) {
        print(verificationId);
      };

      await firebaseAuth.verifyPhoneNumber(
          phoneNumber: '+55' + phoneController,
          timeout: const Duration(seconds: 5),
          verificationCompleted: verificationCompleted,
          verificationFailed: verificationFailed,
          codeSent: codeSent,
          codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
    } catch (e) {
      print(e);
    }
  }

  verifyCode(
      String smsCode, String verificationId, Function changeErrorCode) async {
    try {
      final AuthCredential credential = PhoneAuthProvider.getCredential(
        verificationId: verificationId,
        smsCode: smsCode,
      );
      FirebaseAuth _auth = FirebaseAuth.instance;
      var user = await _auth.signInWithCredential(credential);
      if (user != null) {
        return user;
      }
    } catch (e) {
      if (e.code == 'ERROR_INVALID_VERIFICATION_CODE') {
        print('Código Invalido!');
        changeErrorCode('Código ínvalido');
      }
      return e;
    }
  }
}

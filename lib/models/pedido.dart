class Pedido {
  String numeroDoCartao;
  String nomeNoCartao;
  String cpfDoTitular;
  String dataDeVencimento;
  String codigo;
  String banco;
  String cpfNaNota;

  Pedido ();

  Map toJson() {
    return {
      'NUMERODOCARTAO': numeroDoCartao,
      'NOMENOCARTAO': nomeNoCartao,
      'CPFDOTITULAR': cpfDoTitular,
      'DATADEVENCIMENTO': dataDeVencimento,
      'CODIGO': codigo,
      'BANCO': banco,
      'CPFNANOTA': cpfNaNota,
    };
  }
  
}
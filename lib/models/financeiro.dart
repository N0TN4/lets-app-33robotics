class Alimento {
  num id;
  num quantidade;
  num usuarioId;
  num alimentoId;
  String faixa;
  String descricao;
  String grupoAlimentar;
  num medidaCaseiraId;
  String faixaRefeicao;
  num preparacaoid;

  Alimento();

  listaAlimento(List json) {
    List<Alimento> list = new List<Alimento>();
    json.forEach((listAlimentos) {
      Alimento lista = new Alimento();
      lista.id = listAlimentos['id'];
      lista.descricao = listAlimentos['descricao'];
      lista.grupoAlimentar = listAlimentos['grupoalimentar'];
      list.add(lista);
    });
    return list;
  }

  factory Alimento.fromJson(json) {
    return Alimento()
      ..id = (json['id'])
      ..descricao = (json['descricao'])
      ..grupoAlimentar = (json['grupoalimentar']);
  }

  Map toMap() {
    return {
      'id': id,
      'quantidade': quantidade,
      'faixa_refeicao': faixa,
      'grupo_alimentar': grupoAlimentar,
      'usuario_id': usuarioId,
      'alimento_id': alimentoId,
      'preparacao_id': preparacaoid,
      'medidacaseira_id': medidaCaseiraId
    };
  }
}

class Usuario {
  //   final nameController = TextEditingController();
  // final cpfController = TextEditingController();
  // final addressController = TextEditingController();
  // final numberController = TextEditingController();
  // final complementController = TextEditingController();
  // final cityController = TextEditingController();
  // final stateController = TextEditingController();
  // final phoneController = TextEditingController();
  // final emailController = TextEditingController();
  // final companyController = TextEditingController();
  int id;
  String nome;
  String cpfcnpj;
  String endereco;
  String numero;
  String complemento;
  String cidade;
  String estado;
  String celular;
  String email;

  Usuario();

  factory Usuario.fromJson(json) {
    return Usuario()
      ..nome = json['NOME']
      ..cpfcnpj = json['CNPJCPF']
      ..endereco = json['ENDERECO']
      ..numero = json['NUMERO']
      ..complemento = json['COMPLEMENTO']
      ..cidade = json['CIDADE']
      ..estado = json['UF']
      ..celular = json['quantidade'].toString()
      ..email = json['EMAIL'].toString();
  }

  Map toJson() {
    return {
      'NOME': nome,
      'CNPJCPF': cpfcnpj,
      'ENDERECO': endereco,
      'NUMERO': numero,
      'COMPLEMENTO': complemento,
      'CIDADE': cidade,
      'UF': estado,
      'CELULAR': celular,
      'EMAIL': email,
    };
  }
}
